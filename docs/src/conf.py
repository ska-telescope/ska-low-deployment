# import os
# import sys

# sys.path.insert(0, os.path.abspath('../../src'))

project = 'ska-low-deployment'
copyright = '2024, CSIRO'
author = 'Drew Devereux <drew.devereux@skao.int>'

# The full version, including alpha/beta/rc tags
release = '0.0.0'

extensions = [
    # "sphinx.ext.intersphinx",
    "ska_ser_sphinx_theme",
]

html_theme = 'ska_ser_sphinx_theme'

# intersphinx_mapping = {'python': ('https://docs.python.org/3.10', None)}
