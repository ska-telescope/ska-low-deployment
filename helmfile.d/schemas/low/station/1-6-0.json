{
    "$schema": "http://json-schema.org/schema#",
    "$comment": "schema for telmodel data for a Low station",
    "type": "object",
    "properties": {
        "$schema": {
            "type": "string"
        },
        "platform": {
            "type": "object",
            "properties": {
                "stations": {
                    "type": "object",
                    "additionalProperties": {
                        "type": "object",
                        "propertyNames": {
                            "format": "hostname"
                        },
                        "properties": {
                            "id": {
                                "type": "integer",
                                "minimumValue": 1
                            },
                            "reference": {
                                "type": "object",
                                "properties": {
                                    "datum": {
                                        "type": "string"
                                    },
                                    "epoch": {
                                        "type": "number",
                                        "minimumValue": 2020
                                    },
                                    "latitude": {
                                        "type": "number",
                                        "minimumValue": -90,
                                        "maximumValue": 90
                                    },
                                    "longitude": {
                                        "type": "number",
                                        "minimumValue": 0,
                                        "exclusiveMaximumValue": 360
                                    },
                                    "ellipsoidal_height": {
                                        "type": "number"
                                    }
                                },
                                "additionalProperties": false,
                                "required": [
                                    "datum",
                                    "epoch",
                                    "latitude",
                                    "longitude",
                                    "ellipsoidal_height"
                                ]
                            },
                            "rotation": {
                                "type": "number",
                                "minimumValue": 0.0,
                                "exclusiveMaximumValue": 360.0
                            },
                            "sps": {
                                "type": "object",
                                "properties": {
                                    "csp_ingest_ip": {
                                        "type": "string",
                                        "format": "ipv4"
                                    },
                                    "daq": {
                                        "comment": "Only used for unmanaged DAQs",
                                        "type": "object",
                                        "properties": {
                                            "ip": {
                                                "type": "string",
                                                "format": "ipv4"
                                            },
                                            "port": {
                                                "type": "integer",
                                                "minimumValue": 1
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": [
                                            "ip"
                                        ]
                                    },
                                    "sdn_first_interface": {
                                        "type": "string"
                                    },
                                    "sdn_gateway": {
                                        "type": "string",
                                        "format": "ipv4"
                                    },
                                    "subracks": {
                                        "type": "object",
                                        "additionalProperties": {
                                            "propertyNames": {
                                                "format": "hostname"
                                            },
                                            "type": "object",
                                            "properties": {
                                                "srmb_host": {
                                                    "type": "string",
                                                    "format": "hostname"
                                                },
                                                "srmb_port": {
                                                    "type": "integer",
                                                    "minumumValue": 1
                                                },
                                                "pdu": {
                                                    "type": "string",
                                                    "format": "hostname"
                                                },
                                                "pdu_ports": {
                                                    "type": "array",
                                                    "minItems": 1,
                                                    "maxItems": 2,
                                                    "items": {
                                                        "type": "integer",
                                                        "minimumValue": 1
                                                    },
                                                    "uniqueItems": true
                                                },
                                                "nodeSelector": {
                                                    "type": "object",
                                                    "additionalProperties": {
                                                        "type": "string"
                                                    }
                                                }
                                            },
                                            "additionalProperties": false,
                                            "required": [
                                                "srmb_host",
                                                "srmb_port"
                                            ]
                                        }
                                    },
                                    "tpms": {
                                        "type": "object",
                                        "additionalProperties": {
                                            "propertyNames": {
                                                "format": "hostname"
                                            },
                                            "type": "object",
                                            "properties": {
                                                "id": {
                                                    "type": "integer",
                                                    "minimumValue": 0,
                                                    "maximumValue": 15
                                                },
                                                "host": {
                                                    "type": "string",
                                                    "format": "hostname"
                                                },
                                                "port": {
                                                    "type": "integer",
                                                    "minumumValue": 1
                                                },
                                                "version": {
                                                    "type": "string"
                                                },
                                                "subrack": {
                                                    "type": "string",
                                                    "format": "hostname"
                                                },
                                                "subrack_slot": {
                                                    "type": "integer",
                                                    "minimumValue": 1,
                                                    "maximumValue": 8
                                                }
                                            },
                                            "aditionalProperties": false,
                                            "required": [
                                                "id",
                                                "host",
                                                "port",
                                                "version",
                                                "subrack",
                                                "subrack_slot"
                                            ]
                                        }
                                    }
                                },
                                "additionalProperties": false,
                                "required": []
                            },
                            "pasd": {
                                "type": "object",
                                "properties": {
                                    "fndh": {
                                        "type": "object",
                                        "properties": {
                                            "gateway": {
                                                "type": "object",
                                                "properties": {
                                                    "host": {
                                                        "type": "string",
                                                        "format": "hostname"
                                                    },
                                                    "port": {
                                                        "type": "integer",
                                                        "minimumValue": 1
                                                    },
                                                    "timeout": {
                                                        "type": "number",
                                                        "minimumValue": 0
                                                    }
                                                },
                                                "additionalProperties": false,
                                                "required": [
                                                    "host",
                                                    "port"
                                                ]
                                            },
                                            "controller": {
                                                "type": "object",
                                                "properties": {
                                                    "modbus_id": {
                                                        "type": "integer",
                                                        "minimumValue": 0
                                                    }
                                                },
                                                "additionalProperties": false,
                                                "required": [
                                                    "modbus_id"
                                                ]
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": [
                                            "gateway",
                                            "controller"
                                        ]
                                    },
                                    "smartboxes": {
                                        "type": "object",
                                        "additionalProperties": {
                                            "propertyNames": {
                                                "format": "hostname"
                                            },
                                            "type": "object",
                                            "properties": {
                                                "fndh_port": {
                                                    "type": "integer",
                                                    "minimumValue": 1,
                                                    "maximumValue": 28
                                                },
                                                "modbus_id": {
                                                    "type": "integer",
                                                    "minimumValue": 0
                                                }
                                            },
                                            "additionalProperties": false,
                                            "required": [
                                                "fndh_port",
                                                "modbus_id"
                                            ]
                                        }
                                    }
                                },
                                "additionalProperties": false
                            },
                            "antennas": {
                                "type": "object",
                                "additionalProperties": {
                                    "propertyNames": {
                                        "type": "string",
                                        "format": "hostname"
                                    },
                                    "type": "object",
                                    "properties": {
                                        "masked": {
                                            "type": "boolean",
                                            "default": false
                                        },
                                        "position": {
                                            "type": "object",
                                            "properties": {
                                                "latitude": {
                                                    "type": "number",
                                                    "minimumValue": -90,
                                                    "maximumValue": 90
                                                },
                                                "longitude": {
                                                    "type": "number",
                                                    "minimumValue": 0,
                                                    "exclusiveMaximumValue": 360
                                                }
                                            },
                                            "additionalProperties": false,
                                            "required": [
                                                "latitude",
                                                "longitude"
                                            ]
                                        },
                                        "location_offset": {
                                            "type": "object",
                                            "properties": {
                                                "east": {
                                                    "type": "number"
                                                },
                                                "north": {
                                                    "type": "number"
                                                },
                                                "up": {
                                                    "type": "number"
                                                }
                                            },
                                            "additionalProperties": false,
                                            "required": [
                                                "east",
                                                "north",
                                                "up"
                                            ]
                                        },
                                        "eep": {
                                            "type": "integer",
                                            "minimumValue": 1
                                        },
                                        "smartbox": {
                                            "type": "string",
                                            "format": "hostname"
                                        },
                                        "smartbox_port": {
                                            "type": "integer",
                                            "minimumValue": 1,
                                            "maximumValue": 12
                                        },
                                        "tpm": {
                                            "type": "string",
                                            "format": "hostname"
                                        },
                                        "tpm_fibre_input": {
                                            "type": "integer",
                                            "minimumValue": 1,
                                            "maximumValue": 16
                                        },
                                        "tpm_x_channel": {
                                            "type": "integer",
                                            "minimumValue": 0,
                                            "maximumValue": 31,
                                            "multipleOf": 2
                                        },
                                        "tpm_y_channel": {
                                            "type": "integer",
                                            "minimumValue": 0,
                                            "maximumValue": 31,
                                            "not": {
                                                "multipleOf": 2
                                            }
                                        },
                                        "delay": {
                                            "type": "integer"
                                        },
                                        "delay_x": {
                                            "type": "number"
                                        },
                                        "delay_y": {
                                            "type": "number"
                                        },
                                        "attenuation_x": {
                                            "type": "number",
                                            "minimumValue": 0.0,
                                            "maximumValue": 31.75
                                        },
                                        "attenuation_y": {
                                            "type": "number",
                                            "minimumValue": 0.0,
                                            "maximumValue": 31.75
                                        }
                                    },
                                    "additionalProperties": false,
                                    "$comment": "TODO: Make position required once updated to 0.9.0",
                                    "required": [
                                        "location_offset",
                                        "eep",
                                        "tpm",
                                        "tpm_fibre_input",
                                        "tpm_x_channel",
                                        "tpm_y_channel"
                                    ]
                                }
                            }
                        },
                        "additionalProperties": false,
                        "required": [
                            "id",
                            "rotation",
                            "sps"
                        ]
                    }
                }
            },
            "additionalProperties": false,
            "required": [
                "stations"
            ]
        }
    },
    "additionalProperties": false,
    "required": [
        "platform"
    ]
}