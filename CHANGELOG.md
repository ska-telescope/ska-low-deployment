# Version History

## unreleased

* [LOW-774] Bootstrap repo
* [THORN-27] Set bandpass daqs to have the property `dedicated_bandpass` set to `true` by default.
